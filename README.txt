INTRODUCTION
------------

This module provides Laravel's Illuminate Collections functionality into
Drupal using the tightenco/collect library.

RECOMMENDED MODULES
-------------------

* Composer Manager (https://www.drupal.org/project/composer_manager)
  Provides a UI and autoloading for Composer dependencies for all modules.

REQUIREMENTS
------------

This module requires Composer to download the library.

INSTALLATION
------------

* Enable the module.
* Run `composer install` to download the tighten/collect library from
  Packagist.
* Ensure that you are including autoload.php somewhere (this is done for you
  with Composer Manager).
* Use the `collect()` function on an array to create a collection.

MAINTAINERS
-----------

Current maintainers:
 * Oliver Davies (opdavies) - https://www.drupal.org/u/opdavies
